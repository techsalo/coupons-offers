'use strict';

//var dynamo = require('dynamodb');
//dynamo.AWS.config.loadFromPath('credentials.json');

const AWS     = require('aws-sdk');  
const dynamo  = new AWS.DynamoDB.DocumentClient();
const request = require('request');

const gcmObject = {

    "appId": "2bff8300-f486-4460-9c67-c5872358d359",
    "url": "https://onesignal.com/api/v1/notifications",
    "authHeader": "ZDBiYTFhZDAtMTQyOC00ZTkzLTkxOWQtNGViNDM3MzlkNmE1",
    "contentIcon":"",
    "large_icon":"",
    "small_icon":"",   
    "contents":{"en":""},
    "headings":{"en":"VIRE BILL"},
    "data": {"typeOfNotification": 'cust_bill'},
    "lightColor":"red",
    "deviceIdList":[]
}

let requestObj=
{
    uri: gcmObject.url ,
    method: "POST",
    json: {
        "app_id": gcmObject.appId,
        "contents":gcmObject.contents,
        "include_player_ids":gcmObject.deviceIdList,  // here list of id's will be replaced
        "big_picture": gcmObject.contentIcon,
        "Action Buttons":[],
        "large_icon": gcmObject.large_icon,
        "small_icon": gcmObject.small_icon,
        "headings": gcmObject.headings,
        "led_color":gcmObject.lightColor,
        "data": gcmObject.data
    },
    headers: { //We can define headers too
        'Content-Type': 'application/json',
        'Authorization': gcmObject["authHeader"]
    }
}



function sendNotificationToUser(mobile,companyID,callback){

  let response2 = {
    statusCode: 200,
    body: {
      message: 'Notification sent successfully!',
      status: 0,
      err:"-",
      DID:"-",
      result:"-"
    }
  };

  const gJson={
          TableName:'customers_info',
          Key:{"mobile":mobile}
        }

  dynamo.get(gJson,function(err,result){
      

      const DID=result.Item.did;
      requestObj["json"]["include_player_ids"][0] = DID;
      requestObj["json"]["contents"]["en"] = companyID;
       
      request(requestObj, function(error, response, body) {
      
            if(error || body["errors"] && body["errors"].length>0){
                if(!error){
                    var msg="Error in sending notiifcation to subscriber due to  "+body["errors"][0];
                    response2.body.err = msg;
                }else{
                  response2.body.err = "Error in sending notiifcation";
                }
                console.log("error in notf");
                callback(response2,null);
            }
            else{
                response2.body.status = 1;
                console.log("success in notf");
                callback(null,response2);
            }
        });

  });
}
/*

*/

module.exports.getCoupons = (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
      input: event,
    }),
  };

  callback(null, response);

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};


module.exports.addCoupon = (event, context, callback) => {
  const requestBody = JSON.parse(event.body);
  const companyID = requestBody.companyid;
  const companyName = requestBody.companyname;
  const couponId = requestBody.couponid;
  const couponCode = requestBody.code;
  const couponMsg= requestBody.msg;
  const couponUrl = requestBody.url;
  const couponType=requestBody.coupontype;
  const validStoresArray=requestBody.vstores;
  const validFrom=requestBody.vfrom;
  const validTill= requestBody.vtill;


  var UTC = new Date();
  var IST = new Date(UTC); // Clone UTC Timestamp
  IST.setHours(IST.getHours() + 5); // set Hours to 5 hours later
  IST.setMinutes(IST.getMinutes() + 30);
  const stime=IST.toString();

  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    }
  };

  const cJson={
          TableName:'coupons',
          ReturnValues: "ALL_OLD",
          Item:{
              "coupon_id": couponId,
              "company_id": companyID,
              "company_name": companyName,
              "coupon_dtl": {
                "code": couponCode,
                "msg": couponMsg,
                "url": couponUrl
              },
              "coupon_type": couponType,
              "timestamp": stime,
              "valid_stores": validStoresArray,
              "vFrom": validFrom,
              "vUpto": validTill
          }
  };

  dynamo.put(cJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      callback(null, response);
  });

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};


module.exports.addCompanyAndStores = (event, context, callback) => {
  const requestBody = JSON.parse(event.body);
  const companyID = requestBody.companyid;
  const companyAddress= requestBody.companyaddr;
  const companyName = requestBody.companyname;
  const storesArray= requestBody.stores;



  var UTC = new Date();
  var IST = new Date(UTC); // Clone UTC Timestamp
  IST.setHours(IST.getHours() + 5); // set Hours to 5 hours later
  IST.setMinutes(IST.getMinutes() + 30);
  const stime=IST.toString();

  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    }
  };

  const cJson={
          TableName:'company_stores_mapping',
          ReturnValues: "ALL_OLD",
          Item:{
          
            "company_id": companyID,
            "company_info": {
              "c_ho_address":companyAddress,
              "c_name": companyName
            },
            "store_ids": storesArray
          
          }
        }

  dynamo.put(cJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      callback(null, response);
  });

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};


module.exports.addStore = (event, context, callback) => {
  const requestBody  = JSON.parse(event.body);
  const storeID      = requestBody.storeid;
  const storeAddress = requestBody.storeaddr;
  const storeBranch  = requestBody.storebranch;
  const storeName    = requestBody.storeName;



  var UTC = new Date();
  var IST = new Date(UTC); // Clone UTC Timestamp
  IST.setHours(IST.getHours() + 5); // set Hours to 5 hours later
  IST.setMinutes(IST.getMinutes() + 30);
  const stime=IST.toString();

  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    }
  };

  const cJson={
          TableName:'store_coupons',
          ReturnValues: "ALL_OLD",
          Item:{
              "store_id": storeID,
              "coupons_id": [],
              "store_info": {
                "s_address": storeAddress,
                "s_branch": storeBranch,
                "s_name": storeName
              },
              "timestamp": stime
        }
  }

  dynamo.put(cJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      callback(null, response);
  });

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};



module.exports.updateCouponsForAStore = (event, context, callback) => {
  const requestBody = JSON.parse(event.body);
  const storeID = requestBody.storeid;
  const validCouponsArray= requestBody.coupons;

  var UTC = new Date();
  var IST = new Date(UTC); // Clone UTC Timestamp
  IST.setHours(IST.getHours() + 5); // set Hours to 5 hours later
  IST.setMinutes(IST.getMinutes() + 30);
  const stime=IST.toString();

  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    },
     headers: {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
    }
  };

  const cJson={
          TableName:'store_coupons',
          ReturnValues: "ALL_OLD",
          Key: { 
           "store_id":storeID
           },
           UpdateExpression: "SET coupons_id = :val1",
           ExpressionAttributeValues: { 
            ":val1": validCouponsArray
           },
           ReturnValues: "ALL_NEW"
        }

  dynamo.update(cJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      callback(null, response);
  });

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};


module.exports.getBillsBasedOnMobile = (event, context, callback) => {
  const startTime=event.headers["statime"];
  const endTime=event.headers["endtime"];
  const mobile=event.pathParameters.mobile;
  let indexJson={
    "TableName": "bills",
    "IndexName": "mobile-bill_time-index",
    "KeyConditionExpression": "mobile = :v_mobile",
    "ExpressionAttributeValues": {
        ":v_mobile": mobile
    },
    "ProjectionExpression": "bill_id,bill_info,company_id,bill_time ",
    "ScanIndexForward": true
  }

  if(startTime){
    indexJson={
    "TableName": "bills",
    "IndexName": "mobile-bill_time-index",
    "KeyConditionExpression": "mobile = :v_mobile AND bill_time BETWEEN :v_time AND :v_etime",
    "ExpressionAttributeValues": {
        ":v_mobile": mobile,
        ":v_time": startTime,
        ":v_etime": endTime
    },
    "ProjectionExpression": "bill_id,bill_info,company_id,bill_time ",
    "ScanIndexForward": true
  }


  }
  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    },
     headers: {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
     // "Access-Control-Allow-Headers" : "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,Cache-Control,statime,endtime'"
    }
  };

  dynamo.query(indexJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      callback(null, response);
  });

/*

  var items=[];
  var queryExecute = function(callback) {
var queryExecute = function(callback) {
        docClient.query(params,function(err,result) {
            if(err) {
                callback(err);
                } else {
                console.log(result)
                items = items.concat(result.Items);
                if(result.LastEvaluatedKey) {
                    params.ExclusiveStartKey = result.LastEvaluatedKey;
                    queryExecute(callback);
                    } else {
                        callback(err,items);
                    }
                }
            });
        }
        queryExecute(callback);
    
  */



  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};



module.exports.getBillsForCompany = (event, context, callback) => {

  const company=event.pathParameters.company;

  const indexJson={
    "TableName": "bills",
    "IndexName": "company_id-bill_time-index",
    "KeyConditionExpression": "company_id = :v_com",
    "ExpressionAttributeValues": {
        ":v_com": company
    },
    "ProjectionExpression": "bill_id,bill_info,company_id,store_id,bill_time ",
    "ScanIndexForward": true
  }
  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    },
     headers: {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : false // Required for cookies, authorization headers with HTTPS
    }
  };

  dynamo.query(indexJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      callback(null, response);
  });

  /*
  var items=[];


  var queryExecute = function(callback) {
        dynamo.query(indexJson,function(err,result) {
            if(err) {
                callback(err,null);
                } else {
                console.log(result)
                items = items.concat(result.Items);
                if(result.LastEvaluatedKey) {
                    indexJson.ExclusiveStartKey = result.LastEvaluatedKey;
                    queryExecute(callback);
                    } else {
                        callback(null,items);
                    }
                }
            });
        }
  queryExecute(callback);

  */

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};





module.exports.getBillsForMobileAndCompany = (event, context, callback) => {
  const company=event.headers["company"];
  const mobile=event.pathParameters.mobile;

  const indexJson={
    "TableName": "bills",
    "IndexName": "mobile-company_id-index",
    "KeyConditionExpression": "mobile = :v_mob AND company_id = :v_com",
    "ExpressionAttributeValues": {
        ":v_mob": mobile,
        ":v_com": company
    },
    "ProjectionExpression": "bill_id,bill_info,company_id,bill_time ",
    "ScanIndexForward": true
  }
  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    },
     headers: {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
    }
  };

  dynamo.query(indexJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      callback(null, response);
  });


};
function ISODateString(d) {
    function pad(n) {return n<10 ? '0'+n : n}
    return d.getUTCFullYear()+'-'
         + pad(d.getUTCMonth()+1)+'-'
         + pad(d.getUTCDate())+'T'
         + pad(d.getUTCHours())+':'
         + pad(d.getUTCMinutes())+':'
         + pad(d.getUTCSeconds())+'Z'
};


module.exports.getAllBillsForTimeRange = (event, context, callback) => {
  //console.log(JSON.stringify(event.headers));
  const company=event.pathParameters.company;
  const startTime= event.headers["statime"];
  const endTime= event.headers["endtime"];
 
  const indexJson={
    "TableName": "bills",
    "IndexName": "company_id-bill_time-index",
    "KeyConditionExpression": "company_id = :v_com AND bill_time BETWEEN :v_time AND :v_etime ",
   // "FilterExpression": "bill_time > :v_time",
    "ExpressionAttributeValues": {
        ":v_com": company,
        ":v_time": startTime,
        ":v_etime": endTime
    },
    "ProjectionExpression": "bill_id,bill_info,company_id,store_id,bill_time ",
    "ScanIndexForward": false
  }
  console.log("=======>. "+JSON.stringify(indexJson));
  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    },
     headers: {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
    }
  };

  dynamo.query(indexJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      callback(null, response);
  });



  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};

module.exports.generateBill = (event, context, callback) => {
  const d = new Date();
  const requestBody  = JSON.parse(event.body);
  const billID = d.valueOf()+"-"+requestBody.storeid+"-"+requestBody.company;
  const storeID      = requestBody.storeid;

  const mobile  = requestBody.mobile;
  const billInfo = requestBody.billinfo;
  const companyID=requestBody.company;


  
  const stime=ISODateString(d);

  let response = {
    statusCode: 200,
    body: {
      message: 'Command executed successfully!',
      status: 1,
      err:"-",
      result:"-"
    },
     headers: {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
    }
  };

  const cJson={
          TableName:'bills',
          ReturnValues: "ALL_OLD",
          Item:{
            "bill_id": billID,
            "bill_info": billInfo,
            "company_id": companyID,
            "mobile": mobile,
            "store_id": storeID,
            "bill_time": stime

        }
  }

  dynamo.put(cJson,function(err,result){
      response.body.err=err;
      response.body.result=result;
      response.body = JSON.stringify(response.body);
      if(err){
        callback(err, null);
      }
      else{
          sendNotificationToUser(mobile,companyID,function(err,result){
            callback(null, response);
          });
          
      }
   
  });


 


  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};


// COMPANY STORE LIST
module.exports.getStoresList = (event, context, callback) => {
    //console.log(JSON.stringify(event.headers));

    let response = {
        statusCode: 200,
        body: {
            message: 'Command executed successfully!',
            status: 1,
            err:"-",
            result:"-"
        },
        headers: {
            "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
            "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
        }
    };


    const company=event.pathParameters.company;
    //const startTime= event.headers["statime"];
    //const endTime= event.headers["endtime"];
    const gJson={
        TableName:'company_stores_mapping',
        Key:{"company_id":company}
    }

    dynamo.get(gJson,function(err,result){
        console.log(result);
        if(err){
            response.body.status = 0;
            response.body.err = err;
            callback(response);
        }else{
            response.body.result = result;
            callback(null,response);
        }
    });


    // Use this code if you don't use the http event with the LAMBDA-PROXY integration
    // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};
